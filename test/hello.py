import pygame
import random

pygame.init()

pygame.display.set_caption("Hello Pygame!")
window = pygame.display.set_mode((640,480))

running = True
clock = pygame.time.Clock()

position = [320, 240]

while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
            break
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                position[0] = random.randint(20, 620)
                position[1] = random.randint(20, 460)
                break

    window.fill((30,30,100))
    pygame.draw.circle(window, (200, 50, 50), position, 20)
    pygame.display.update()

    clock.tick(60)

pygame.quit()